## Założenia techniczne.
* Klient / sewer pracujący na socketach.

* Informacje o nawiązanych połączeniach, kolizjach i udanych lądowaniach zapisujesz w bazie.

* Klienty to samoloty

* Serwer to system automatycznego lądowania.


## Założenia biznesowe
1. Lotnisko posiada 2 pasy

2. W przestrzeni powietrznej lotniska może się znajdować maksymalnie 100 samolotów, każdy kolejny, w trakcie próby połączenia powinien otrzymać informację o konieczności znalezienia sobie innego lotniska i znika z twojego "radaru"

3. Lotnisko zawiaduje przestrzenią 10 km na 10 km i wysokości 5 km.

4. Każdy przylatujący samolot ma paliwa na 3 godziny lotu

5. Należy ustalić korytarze powietrzne służące do lądowania

6. Samoloty pojawiają się w losowym miejscu na granicy obszaru powietrznego, POZA korytarzami powietrznymi na wysokości 2km do 5km

7. Sprowadzenie samolotu na ziemię polega na skierowaniu go do korytarza powietrznego i pilnowania jego czystej drogi do pasa startowego, na pasie startowym samolot ma mieć wysokość równą 0m inaczej lądowanie jest uznane za nieudane i następuje kolizja

8. Jeśli dwa samoloty znajdą się w odległości 10m od siebie, uznajemy, że nastąpiła kolizja

9. Jeśli w samolocie podczas oczekiwania skończy się paliwo, uznajemy, że nastąpiła kolizja