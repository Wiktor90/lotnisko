
from airport.radar import Radar
from handlers import *
from services import *


logger = logger()
radar = Radar()
connection, address = run_server()

with connection as conn:
    logger.info(f"Connected by: {address[0]} | {address[1]}")

    payload: dict = recv_msg(conn)

    cube_index = int(payload["current_cube"])
    permission: bool = not radar.can_plane_enter_hub(cube_index)
    update_payload(payload, refused=permission)
    send_msg(conn, payload)

    # STANDARD PlANE COMMUNICATION (checking and updating payload)
    while True:
        payload = recv_msg(conn)
        if payload is None:
            logging.info("Connection Closed")
            break

        cube_index = int(payload["current_cube"])
        moving_instruction(radar, cube_index, payload)
        set_landing_status(radar, cube_index, payload)
        send_msg(conn, payload)


# SERVER-------
# import socket
# import threading
#
# HEADER = 64
# PORT = 5050
# SERVER = socket.gethostbyname(socket.gethostname())
# ADDR = (SERVER, PORT)
# FORMAT = 'utf-8'
# DISCONNECT_MESSAGE = "!DISCONNECT"
#
# server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# server.bind(ADDR)
#
#
# def handle_client(conn, addr):
#     print(f"[NEW CONNECTION] {addr} connected.")
#
#     connected = True
#     while connected:
#         msg_length = conn.recv(HEADER).decode(FORMAT)
#         if msg_length:
#             msg_length = int(msg_length)
#             msg = conn.recv(msg_length).decode(FORMAT)
#             if msg == DISCONNECT_MESSAGE:
#                 connected = False
#
#             print(f"[{addr}] {msg}")
#             conn.send("Msg received".encode(FORMAT))
#
#     conn.close()
#
#
# def start():
#     server.listen()
#     print(f"[LISTENING] Server is listening on {SERVER}")
#     while True:
#         conn, addr = server.accept()
#         thread = threading.Thread(target=handle_client, args=(conn, addr))
#         thread.start()
#         print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")
#
#
# print("[STARTING] server is starting...")
# start()


# CLIENT ------
# import socket
#
# HEADER = 64
# PORT = 5050
# FORMAT = 'utf-8'
# DISCONNECT_MESSAGE = "!DISCONNECT"
# SERVER = "192.168.1.26"
# ADDR = (SERVER, PORT)
#
# client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# client.connect(ADDR)
#
# def send(msg):
#     message = msg.encode(FORMAT)
#     msg_length = len(message)
#     send_length = str(msg_length).encode(FORMAT)
#     send_length += b' ' * (HEADER - len(send_length))
#     client.send(send_length)
#     client.send(message)
#     print(client.recv(2048).decode(FORMAT))
#
# send("Hello World!")
# input()
# send("Hello Everyone!")
# input()
# send("Hello Tim!")
#
# send(DISCONNECT_MESSAGE)