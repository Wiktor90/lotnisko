
from airport.plane import Plane
from handlers import *
from services import *


logger = logger()
plane = Plane()
connection = establish_client()

with connection as conn:
    payload: dict = generate_payload(plane)

    while True:
        send_msg(conn, payload)
        payload: dict = recv_msg(conn)
        if should_close_connection(payload):  # False by default
            break
        logger.debug(f'--> Plane: {plane.id} in cube - {payload["current_cube"]}')

    break_reason = [(k, v) for k, v in payload.items() if v is True]
    logger.info(f'CONNECTION END -> REASON: {break_reason[0]}')
