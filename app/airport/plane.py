import logging
import time


from datetime import datetime
from uuid import uuid4
from .space import Space


class Plane:
    @staticmethod
    def get_entry_cube() -> tuple:
        """random choice of one of marginal cube from space"""

        space = Space()
        cube = space.starting_cube
        return cube["index"], cube["coords"]

    def __init__(self):
        self.id = str(uuid4().fields[-1])[:5]
        self.entry_position = Plane.get_entry_cube()
        self.current_position = self.entry_position
        self.fuel_time = "03:00:00"
        self.landing_permission = False
        self.is_landed = False
        self.is_crashed = False

    def set_current_position(self, cube):
        """set in which cube is plane currently"""
        self.current_position = cube["index"], cube["coords"]
        return self.current_position

    def lower_fuel_time(self, minutes: str, f="%H:%M:%S") -> str:
        self.fuel_time = datetime.strptime(self.fuel_time, f)
        minutes = datetime.strptime(minutes, f)
        sec = (self.fuel_time - minutes).total_seconds()
        self.fuel_time = time.strftime(f, time.gmtime(sec))
        return self.fuel_time

    def is_crashed_by_fuel_time(self):
        """
        If fuel_time is out of 3H time-box that means plane is crashed
        """

        allowed_hours = ["00", "01", "02", "03"]
        if self.fuel_time[0:2] not in allowed_hours:
            logging.error(f'Plane id: {self.id} is CRASHED !')
            self.is_crashed = True
        return self.is_crashed

    def landing(self):
        # TODO: it's a Draft. Is here for future remaining time reasons.
        if self.landing_permission:
            self.is_landed = True
