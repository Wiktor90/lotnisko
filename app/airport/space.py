import random


class Space:
    @staticmethod
    def coords_generator() -> list:
        """
        Generate 100 coords in the row
        - from W(x)1 - L(y)1
        - to W(x)10 - L(y)10
        - H = 5 for dirty MVP reasons now
        single coord pattern -> dict(): "{"W": 1, "L": 1, "H": 5}"
        """
        coords = [[dict(W=j+1, L=i+1, H=5) for j in range(10)] for i in range(10)]
        return coords

    @staticmethod
    def match_coords_by_sector() -> list:
        """
        :return: 0-99 coords in snake order:
        {W:1,L:1}, {W:2,L:1} ... {W:10,L:1}, {W:10,L:2}, {W:9,L:2} ... {W:1,L:2}
        """
        matched = []
        coords: list = Space.coords_generator()
        for coord in coords:
            matched.append([coord[i] for i in range(len(coord))])

        for i in range(len(matched)):
            if i % 2 != 0:
                matched[i] = matched[i][::-1]
        return sum(matched, [])

    @staticmethod
    def air_cubes_generator() -> list:
        coords = Space.match_coords_by_sector()
        cube_list = [
            dict(
                index=i,
                coords=coords[i],
                free=True,
            )
            for i in range(len(coords))
        ]
        return cube_list

    @staticmethod
    def get_marginal_cubes(cubes) -> list:
        sides = (1, 10)
        marginal = lambda cube: list(set(cube["coords"].values()).intersection(sides))
        return list(filter(marginal, cubes))

    def __init__(self):
        self.cubes: list = Space.air_cubes_generator()
        self.starting_cubes = Space.get_marginal_cubes(self.cubes)

    def cube_status(self, cube_index: int) -> bool:
        return self.cubes[cube_index]["free"]

    @property
    def starting_cube(self):
        return random.choice(self.starting_cubes)

# TODO:
# Refactor of matching cubes needed !
