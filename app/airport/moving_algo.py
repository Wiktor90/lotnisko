def direction(cube_index: int) -> str:
    """
    return direction deciding if plane should move +1 cube or -1 cube
    :param cube_index - number of cube from Space cubes list
    :return: "+" / "-" - > str
    """

    directions = ["+", "-"]
    plus = 44 > cube_index
    plus2 = 50 <= cube_index <= 54
    plus_range = any([plus, plus2])
    return directions[0] if plus_range else directions[1]


def count_target(index, sign):
    if sign == "+":
        return index + 1
    return index - 1


def get_target_index(cubes: list, cube_index: int):
    """
    :param cubes: list of all cubes in space
    :param cube_index: cube index of current plane position
    :return: index of target cube where plane will change position,
    it is a "target param for radar.change_position func.
    """

    cube: dict = cubes[cube_index]
    sign: str = direction(cube["index"])
    target_cube_index = count_target(cube["index"], sign)
    return target_cube_index
