import logging
from typing import Union

from .hub import Airport
from .moving_algo import get_target_index
from .space import Space


class Radar:
    def __init__(self):
        self.space = Space()
        self.hub = Airport()
        self.cubes = self.space.cubes
        self.landing_cubes = [44, 54]

    def set_cube_occupied(self, cube_index: int) -> dict:
        cube_occupied = self.cubes[cube_index]
        cube_occupied["free"] = False
        return cube_occupied

    def reset_cube(self, current_position: int) -> dict:
        """return whole cube object"""
        cube_to_reset = self.cubes[current_position]
        cube_to_reset["free"] = True
        return cube_to_reset

    def cube_status(self, cube_index: int) -> dict:
        return self.cubes[cube_index]["free"]

    def can_plane_enter_hub(self, cube_index: int) -> bool:
        """
        check if all conditions that should be passed to
        plane could enter the air space
        """
        is_capacity = self.hub.capacity < 100
        is_entry_cube_free = self.cubes[cube_index]["free"]
        return all([is_capacity, is_entry_cube_free])

    def enter_to_hub(self, cube_index: int) -> bool:
        """
        :return T/F if plane is able to enter the air space
        change enter cube status free on False
        """
        if self.can_plane_enter_hub(cube_index):
            self.hub.increase_capacity()
            self.set_cube_occupied(cube_index)
            logging.warning(f'Cube {self.cubes[cube_index]["index"]}:{self.cubes[cube_index]["coords"]} is occupied')
            return True
        logging.warning("Airport is full or entry cube owned")
        return False

    def is_move_possible(self, target: int):
        """
        :param target: index of cube which will be next position
        :return: is this cube occupied or free (T/F)
        """
        target_cube = self.cubes[target]
        return True if target_cube["free"] else False

    def change_position(self, current_position: int, target: int) -> dict:
        """return whole cube object"""
        self.reset_cube(current_position)
        new_position = self.set_cube_occupied(target)
        return new_position

    def move_or_wait(self, current_position: int) -> Union[dict, str]:
        """
        return whole new_cube object or
        str if move to target is not possible
        """
        target = get_target_index(self.cubes, current_position)
        if self.is_move_possible(target):
            return self.change_position(current_position, target)
        else:
            return "WAIT"

    def can_plain_land(self, cube_index: int) -> bool:
        """check if plane is in landing cubes (T/F)"""
        if cube_index in self.landing_cubes:
            return True
        return False


# KOMUNIKACJA
# [x] - client musi byc klasa samolotu z zainicjalizowana pozycja startowa
# [x] - client pyta server czy moze wleciec na dane koordynaty
# [x] - server odpowiada tak / lub zrywa polaczenia
    # [x] - standardowa komunikacja
    # [x] - jesli odrzuc to zamknij polaczenie
# [] - Client pyta czy ma wykonac ruch
    # [] - jesli tak to gdzie - obsluga watku, sprawdzeni, blokada, zmiana statusu cuba
    # [] - jesli nie to czekaj
    # [] - kazda akcja powinna konsumowac czas
# [] - format komunikacji JSON - glownie czes / id cuba / coordynaty / status landed / crashed
# [] - jezeli status landed / crashed is TRUE to zamknij polaczenie
    # [] - obsluga pasow ladowania - zmiana statusu

# [] - obsluzyc 1 samolot
