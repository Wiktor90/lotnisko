import logging


class Airport:
    def __init__(self):
        self.space = dict(L=10000, W=10000, H=5000,)
        self.capacity = 0
        self.__airstrips = dict(A=False, B=False)

    def increase_capacity(self) -> int:
        if self.capacity < 100:
            self.capacity += 1
            logging.warning(f'Airport capacity: {self.capacity}')
        return self.capacity

    def decrease_capacity(self):
        if self.capacity > 0:
            self.capacity -= 1
        return self.capacity

    @property
    def is_airstrip_A_occupied(self) -> bool:
        return self.__airstrips['A']

    @property
    def is_airstrip_B_occupied(self) -> bool:
        return self.__airstrips['B']

    @is_airstrip_A_occupied.setter
    def is_airstrip_A_occupied(self, value: bool):
        self.__airstrips['A'] = value

    @is_airstrip_B_occupied.setter
    def is_airstrip_B_occupied(self, value: bool):
        self.__airstrips['B'] = value
