import logging
import json
import socket


def logger():
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(levelname)s:%(asctime)s:%(name)s:%(message)s"
    )
    return logging.getLogger(__name__)


service_logger = logger()
HOST = "127.0.0.1"
PORT = 8000
CODER = "utf-8"
BUFFER = 1024


def run_server(host=HOST, port=PORT):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    connection, address = s.accept()  # socket ojb to communicate with client
    return connection, address
    # server_sock = socket.socket()
    # server_sock.bind((host, port))
    # server_sock.listen(0)
    # server_sock.accept()
    # server_sock.close()


def establish_client(host=HOST, port=PORT):
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.connect((host, port))
    # with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    #     s.connect((HOST, PORT))
    return connection


def send_msg(conn, msg: dict, coder=CODER):
    msg = json.dumps(msg, indent=2)
    conn.send(msg.encode(coder))


def recv_msg(conn, buffer=BUFFER, coder=CODER):
    data = conn.recv(buffer).decode(coder)
    try:
        data = json.loads(data)
    except json.decoder.JSONDecodeError:
        service_logger.warning(f'Received data are empty.')
        return None
    return data
