# CLIENT HANDLERS ----------------------------------

def generate_payload(plane) -> dict:
    payload = {
        "id": plane.id,
        "refused": False,
        "current_cube": plane.current_position[0],
        "current_position": plane.current_position[1],
        "landing_permission": plane.landing_permission,
        "is_landed": plane.is_landed,
        "is_crashed": plane.is_crashed,
        "remaining_time": plane.fuel_time
    }
    return payload


def should_close_connection(payload) -> bool:
    """
    Check crucial information in the payload, in case of keeping C/S connection.
    is_crashed, is_landed, refused - all should False otherwise,
    connection will be closed
    :param payload: included data about plane instance.
    :return: Bool T/F
    """
    return any([
        payload["refused"],
        payload["is_crashed"],
        payload["is_landed"],
    ])


# SERVER HANDLERS -------------------------------------

def update_payload(payload, **kwargs) -> dict:
    """
    :param payload: included data about plane instance.
    :param kwargs: key=value -> as an update / new value in payload
    :return: dict payload -> side effect if new key it save new value
     in communication payload
    """
    for k, v in {**kwargs}.items():
        payload[k] = v
    return payload


def is_plane_refused(radar, payload: dict) -> bool:
    """
    check that plane can enter the airspace
    :param radar: instance of Radar()
    :param payload: C/S communication json
    :return: T/F value if plane will be further proceed
    """
    cube_index = int(payload["current_cube"])
    return radar.can_plane_enter_hub(cube_index)


def set_landing_status(
        radar,
        cube_index: int,
        payload: dict,
):
    """
    Run radar method to evaluate that plane can land or not.
    Then change is_landing status in payload
    """
    landing_status = radar.can_plain_land(cube_index)
    update_payload(
        payload,
        is_landed=landing_status
    )


def moving_instruction(radar, cube_index: int, payload: dict):
    """
    :param radar: Radar class
    :param cube_index: index of plane current cube
    :param payload: standard communication payload
    :return: None if move_or_wait return str (instruction == WAIT -> cass str),
    If move_or_wait return new_cube:dict
    then func update payload by new target cube data.
    """
    instruction = radar.move_or_wait(cube_index)
    if type(instruction) is str:
        return None
    else:
        current_position: dict = instruction["coords"]  # coords
        current_cube: int = instruction["index"]  # index
        update_payload(
            payload,
            current_position=current_position,
            current_cube=current_cube
        )

