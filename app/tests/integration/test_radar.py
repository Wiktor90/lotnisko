import pytest


def test_can_plane_enter_hub(radar, cube):
    assert radar.can_plane_enter_hub(cube["index"])


def test_changing_cube_status_after_plane_entry(radar, cube):
    assert cube["free"]
    radar.enter_to_hub(cube["index"])
    assert cube["free"] is False


def test_cube_status(radar, cube):
    result = radar.cube_status(cube["index"])
    assert cube["free"] == result

    result = radar.set_cube_occupied(cube["index"])
    assert result["free"] == radar.cube_status(cube["index"])


def test_is_move_possible(radar, cube):
    assert radar.is_move_possible(cube["index"])

    radar.set_cube_occupied(cube["index"])
    assert radar.is_move_possible(cube["index"]) is False


@pytest.mark.parametrize(
    "current_position, target",
    [
        (0, 1),
        (52, 53),
        (57, 56),
        (43, 44),
        (48, 47),
        (99, 98),
    ]
)
def test_plane_can_move_to_target_cube_if_free(
        radar,
        current_position,
        target,
):
    result = radar.move_or_wait(current_position)
    assert result == radar.cubes[target]
    assert radar.cubes[target]["free"] is False


@pytest.mark.parametrize(
    "current_position, target",
    [
        (0, 1),
        (52, 53),
        (57, 56),
        (43, 44),
        (48, 47),
        (99, 98),
    ]
)
def test_plane_wait_if_target_cube_occupied(
        radar,
        current_position,
        target,
):
    radar.set_cube_occupied(target)
    result = radar.move_or_wait(current_position)
    assert result == "WAIT"


def can_plane_land(radar, cube):
    landing_cube = radar.cubes[44]
    status = radar.can_plain_land(landing_cube)
    assert status is True

    status = radar.can_plain_land(cube)
    assert status is False
