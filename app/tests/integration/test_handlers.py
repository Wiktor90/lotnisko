import pytest

from handlers import *


@pytest.mark.parametrize(
    "key, value",
    [
        ("id", 1234),
        ("is_landed", True),
        ("current_cube", 1),
    ]
)
def test_update_payload_ok(payload, key, value):
    args = {key: value}
    update_payload(payload, **args)
    assert payload[key] == value
    assert len(payload.keys()) > 1


def test_should_close_connection(payload):
    keep_conn = should_close_connection(payload)
    assert keep_conn is False

    update_payload(payload, refused=True)
    close_conn = should_close_connection(payload)
    assert close_conn is True


def test_is_landing_status_set_properly(
        payload,
        cube,
        landing_cube,
        radar,
):
    cube_index = cube["index"]
    set_landing_status(radar, cube_index, payload)
    assert payload["is_landed"] is False

    landing_cube_index = landing_cube["index"]
    set_landing_status(radar, landing_cube_index, payload)
    assert payload["is_landed"] is True


def test_payload_is_not_ged_acc_moving_instructions(
        cube,
        radar,
        plane,
):
    # When
    plane.set_current_position(cube)
    payload = generate_payload(plane)
    target_cube_index = cube["index"] + 1
    radar.set_cube_occupied(target_cube_index)
    # Given
    moving_instruction(
        radar,
        plane.current_position[0],
        payload,
    )
    # Then
    assert plane.current_position[0] == cube["index"]
    assert payload["current_cube"] == cube["index"]


def test_payload_is_changed_acc_instructions(
        cube,
        plane,
        radar,
):
    # When
    plane.set_current_position(cube)
    payload = generate_payload(plane)
    # Given
    moving_instruction(
        radar,
        plane.current_position[0],
        payload,
    )
    # Then
    assert payload["current_cube"] == cube["index"]+1
