import threading
import time
import socket

import pytest

from services import run_server, establish_client


# def test_establish_local_connection(server):
#     assert False
#     server_thread = threading.Thread(target=server)
#     server_thread.start()
#     time.sleep(0.2)
#     breakpoint()
#     with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#         s.connect(("127.0.0.1", 8000))
#     server_thread.join()
#


@pytest.fixture()
def server():
    return run_server()


@pytest.fixture()
def client():
    return establish_client()
