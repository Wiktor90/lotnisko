import pytest

from app.airport.moving_algo import direction, get_target_index
from app.airport.space import Space


@pytest.mark.parametrize(
    "starting_list_index, sign",
    [
        (0, "+"),
        (14, "+"),
        (15, "+"),
        (16, "+"),
        (17, "-"),
        (18, "+"),
        (21, "-"),
        (23, "-"),
        (35, "-"),
    ]
)
def test_side_cube_has_proper_direction_sign(
        starting_cubes,
        starting_list_index,
        sign,
):
    cube = starting_cubes[starting_list_index]
    result = direction(cube["index"])
    assert result == sign


@pytest.mark.parametrize(
    "cube_index, expected",
    [
        (0, 1),
        (99, 98),
        (67, 66),
        (23, 24),
        (50, 51),
        (53, 54),
        (49, 48),
        (59, 58),
    ]
)
def test_target_cube_is_count_ok(
        cubes,
        cube_index,
        expected,
):
    result = get_target_index(cubes, cube_index)
    assert result == expected


@pytest.fixture()
def starting_cubes():
    space = Space()
    return space.starting_cubes


@pytest.fixture()
def cubes():
    space = Space()
    return space.cubes
