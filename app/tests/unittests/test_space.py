import pytest


def test_is_cubes_sector_generated_ok(space):
    assert len(space.cubes) == 100
    assert space.cubes[0]["index"] == 0
    assert space.cubes[0]["free"]
    assert space.cubes[-1]["index"] == 99


@pytest.mark.parametrize(
    "cube_id, expected_coord",
    [
        (0,  {"W": 1, "L": 1, "H": 5},),
        (9, {"W": 10, "L": 1, "H": 5},),
        (10, {"W": 10, "L": 2, "H": 5},),
        (24, {"W": 5, "L": 3, "H": 5},),
        (25, {"W": 6, "L": 3, "H": 5},),
        (45, {"W": 6, "L": 5, "H": 5},),
        (50, {"W": 10, "L": 6, "H": 5},),
        (69, {"W": 10, "L": 7, "H": 5},),
        (99, {"W": 1, "L": 10, "H": 5},),

    ]
)
def test_has_cube_proper_coords(
        space,
        cube_id,
        expected_coord,
):
    cube: dict = space.air_cubes_generator()[cube_id]
    assert cube["coords"] == expected_coord


def test_correct_amount_of_starting_cubes(space):
    assert len(space.starting_cubes) == 36


def test_marginal_cubes_have_good_coords(space):
    cubes = space.cubes
    marginal_cubes = [
        cube for cube in cubes
        if 1 in cube["coords"].values() or 10 in cube["coords"].values()
    ]
    assert space.starting_cubes == marginal_cubes
