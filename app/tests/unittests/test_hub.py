import pytest

from app.airport.hub import Airport


@pytest.mark.parametrize(
    "capacity, expected",
    [
        (0, 1),
        (99, 100),
        (100, 100),
    ]
)
def test_increase_capacity(airport, capacity, expected):
    airport.capacity = capacity
    assert airport.increase_capacity() == expected


@pytest.mark.parametrize(
    "capacity, expected",
    [
        (1, 0),
        (0, 0),
        (100, 99),
    ]
)
def test_decrease_capacity(airport, capacity, expected):
    airport.capacity = capacity
    assert airport.decrease_capacity() == expected


def test_works_airstrips_property_ok(airport):
    airport.is_airstrip_A_occupied = True
    assert airport.is_airstrip_A_occupied is True
    assert airport.is_airstrip_B_occupied is False
