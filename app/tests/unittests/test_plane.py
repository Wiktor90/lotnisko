import pytest


@pytest.mark.parametrize(
    "time, remaining",
    [
        ("00:05:00", "02:55:00"),
        ("01:05:00", "01:55:00"),
        ("03:00:00", "00:00:00"),
    ]
)
def test_fuel_time_calculating_ok(plane, time, remaining):
    assert plane.lower_fuel_time(time) == remaining


@pytest.mark.parametrize(
    "time, expected",
    [
        ("01:05:00", False),
        ("23:50:00", True),
    ]
)
def test_change_is_crashed_if_exceeded_time(
        plane,
        time,
        expected
):
    plane.fuel_time = time
    assert plane.is_crashed_by_fuel_time() == expected


@pytest.mark.parametrize(
    "coord",
    ["W", "L", "H"]
)
def test_is_entry_position_correct(plane, coord,):
    position = plane.entry_position
    assert isinstance(position[0], int)
    assert isinstance(position[1], dict)
    assert coord in position[1].keys()


def test_entry_position_equal_to_current_on_start(
        plane,
        cube,
):
    assert plane.entry_position == plane.current_position
    plane.set_current_position(cube)
    assert plane.entry_position != plane.current_position
