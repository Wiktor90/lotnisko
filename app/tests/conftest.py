import pytest

from airport.hub import Airport
from airport.plane import Plane
from airport.radar import Radar
from airport.space import Space
from handlers import generate_payload


@pytest.fixture()
def radar():
    return Radar()


@pytest.fixture
def airport():
    return Airport()


@pytest.fixture
def plane():
    return Plane()


@pytest.fixture
def space():
    return Space()


@pytest.fixture()
def payload(plane):
    starting_payload = generate_payload(plane)
    return starting_payload


@pytest.fixture()
def cube(radar):
    return radar.cubes[18]


@pytest.fixture()
def landing_cube(radar):
    return radar.cubes[44]
